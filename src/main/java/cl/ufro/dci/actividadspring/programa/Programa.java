package cl.ufro.dci.actividadspring.programa;

import cl.ufro.dci.actividadspring.calculo.CalculoVariacion;
import cl.ufro.dci.actividadspring.extractor.Extractor;

import java.util.Scanner;

public class Programa {
    public Programa() {
    }

    public static void run() {
        int opcion;
        Scanner leer = new Scanner(System.in);
        System.out.println("Bienvenido al programa...");
        do {
            System.out.println(
                    "Elija una opcion:\n" +
                            "1) Calcular Variacion Porcentual UF\n" +
                            "0) Salir del Programa\n" +
                            "---------------------------");
            opcion = leer.nextInt();
            if (opcion == 1) {
                System.out.println("El calculo se hace entre el valor del 1 de Enero de este año y una fecha que sea ingresada...\n" +
                        "\n" +
                        "Ingrese un mes:\n" +
                        "1-Enero\n" +
                        "2-Febrero\n" +
                        "3-Marzo\n" +
                        "4-Abril\n" +
                        "5-Mayo\n" +
                        "6-Junio\n" +
                        "7-Julio\n" +
                        "8-Agosto\n" +
                        "9-Septiembre\n" +
                        "10-Octubre\n" +
                        "11-Noviembre\n" +
                        "12-Diciembre");
                int mes = leer.nextInt();
                System.out.println("Seleccione un dia: (del 1-31)");
                int dia = leer.nextInt();
                Extractor extractor = new Extractor();
                System.out.println("--------------------------");
                System.out.println("La variacion es: " +
                        (CalculoVariacion.calculoPorcentajeUF(
                                extractor.obtenerValorUF(1,
                                        extractor.obtenerMes(1)),
                                extractor.obtenerValorUF(dia,
                                        extractor.obtenerMes(mes))
                        )) + "\n--------------------------");
            } else if (opcion == 0) {
                System.out.println("Que tenga un buen dia.");
            }
        } while (opcion != 0);
    }
}
