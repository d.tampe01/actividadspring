package cl.ufro.dci.actividadspring;

import cl.ufro.dci.actividadspring.programa.Programa;

public class App {
    public static void main(String[] args) {
        Programa.run();
    }
}
