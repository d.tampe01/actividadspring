package cl.ufro.dci.actividadspring.extractor;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.time.LocalDateTime;


public class Extractor {
    Document documento;
    String prefijo = "gr_ctl";

    public Extractor() {
        try {
            documento = Jsoup.connect("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=").get();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public float obtenerValorUF(int dia, String mes) {
        String id;
        if (dia >= 10) {
            id = prefijo + (dia + 1) + "_" + mes;
        } else {
            id = prefijo + "0" + (dia + 1) + "_" + mes;
        }
        Element elemento = documento.getElementById(id);
        String valor = elemento.html();
        valor = valor.replace(".", "");
        valor = valor.replace(",", ".");
        return Float.parseFloat(valor);
    }

    public String obtenerMes(int valorMes) {
        switch (valorMes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                throw new IllegalStateException("Valor inesperado: " + valorMes);
        }
    }


    /*
     * Ejemplo de funcionalidad
     */
    //public static void main(String[] args) {
    //   Extractor extractor = new Extractor();
    //  LocalDateTime mes = LocalDateTime.now();
    // int valorMes = mes.getMonthValue();
    // System.out.println(extractor.obtenerValorUF(1, extractor.obtenerMes(valorMes)));
    // }
}
